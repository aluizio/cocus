# Exercise \#1
In order to do the exercise \#1, the file [data.txt](data.txt) was created and filled with 10000 lines from the Lorem Ipsum generator.

## Dependecies

The library **djangorestframework** was used to produce the API response in a valid JSON format. Also, the library **djangorestframework-xml** was used to render the data in XML format.

## Accessing the data

### JSON
To request a random line in JSON format, access:  
http://localhost:8000/random-line/?format=json

### XML
For XML, access:  
http://localhost:8000/random-line/?format=xml

### Default
If no parameter is passed, the browsable API will be rendered. This page provides some useful REST tools:  
http://localhost:8000/random-line/

## Main code
The main code is inside the file [views.py](views.py).