import pytest

from .views import RandomLineView

@pytest.mark.unit
class TestRandomLineView:

    @pytest.fixture
    def view(self):
        return RandomLineView()

    def test_randomlineview_get_when_called_should_return_dict_data(self, view):
        response = view.get(None)
        data = response.data
        assert isinstance(data, dict)
        assert 'line' in data
        assert isinstance(data['line'], str)
        assert len(data.keys()) == 1
