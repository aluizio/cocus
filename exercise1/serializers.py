from rest_framework import serializers


class RandomLineSerializer(serializers.Serializer):
    line = serializers.CharField()
