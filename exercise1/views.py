import os
import linecache
from random import randint

from rest_framework import views
from rest_framework.response import Response

from .serializers import RandomLineSerializer


dir_name = os.path.dirname(__file__)
data_file_name = os.path.join(dir_name, "data.txt")


class RandomLineView(views.APIView):
    MAX_LINE_NUMBER = 10000

    def get(self, request):
        line_number = randint(0,self.MAX_LINE_NUMBER)
        line_data = linecache.getline(data_file_name, line_number)
        data = {
            'line': line_data
        }
        results = RandomLineSerializer(data, many=False).data
        return Response(results)
