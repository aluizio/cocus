from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from .views import RandomLineView

urlpatterns = [
    path('random-line/', RandomLineView.as_view()),
]


"""
    format_suffix_patterns is responsible for
    select the format of the data in response
    and the format can be JSON, XML or a web page with the data
"""
urlpatterns = format_suffix_patterns(urlpatterns)
