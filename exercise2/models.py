from django.db import models

# Create your models here.
class RandomLine(models.Model):
    line = models.TextField(max_length=2000)
    most_frequent_char = models.CharField(max_length=1)
