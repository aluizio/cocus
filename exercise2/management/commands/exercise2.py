import json
import requests

from django.core.management.base import BaseCommand

from exercise2.models import RandomLine


class Command(BaseCommand):
    url = 'http://localhost:8000/random-line/'

    args = ''
    help = f'make a request to {url} and \
            save the response in the data base with \
            the most frequent character'

    @staticmethod
    def get_most_frequent_char(line: str):
        data = {}
        excludes = [" ", "\n"]

        for char in line:
            if char in excludes:
                continue
            if char not in data:
                data[char]=0
            data[char] += 1

        if len(data.keys()) == 0:
            return ""
        
        max_data = max(data.items(), key=lambda x: x[1])
        return max_data[0]

    @staticmethod
    def print_data(data: dict):
        renderer_data = json.dumps(data, indent=4, sort_keys=True)
        print(renderer_data)

    def handle(self, *args, **options):
        response = requests.get(self.url, {'format': 'json'})
        data = response.json()

        data['most_frequent_char'] = self.get_most_frequent_char(data['line'])
        self.print_data(data)

        obj = RandomLine(**data)
        obj.save()
        print(f"Entry saved id: {obj.id}")
