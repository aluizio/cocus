# Exercise \#2

To execute the script inside the Django enviroment, the Django command *exercise2* was created.

## Running the script

If the server is not running, start it with:
 ```
    python manage.py runserver 0.0.0.0:8000
 ```

Then, execute the script with the following command:  
 ```
    python manage.py exercise2
 ```

This script makes a request to http://localhost:8000/random-lines/?format=json with the help of the library **Requests**, then it parses JSON data and store it in the database through Django ORM.

Each time the script is executed, a new line is stored in the database.

## Model
The model called RandomLine was used to store the data in the database. Also, this model was registered in the Django administration tool.

## Main code
The main code is inside the file [management/commands/exercise2.py](management/commands/exercise2.py).