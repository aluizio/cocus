import pytest
from unittest import mock

from .management.commands.exercise2 import Command, requests, RandomLine

@pytest.mark.unit
class TestCommand:

    @pytest.fixture
    def command(self):
        return Command()

    @pytest.fixture
    def response(self):
        data = {
            "line": "Lorem ipsum" # most frequent - m
        }
        resp = mock.Mock()
        resp.json.return_value = data
        return resp

    @pytest.fixture
    def mock_requests(self, response):
        with mock.patch.object(requests, 'get', return_value=response) as mock_method:
            yield mock_method

    @pytest.fixture
    def mock_database_save(self):
        with mock.patch.object(RandomLine, 'save', return_value=None) as mock_method:
            yield mock_method

    @pytest.mark.parametrize("line,expected_char", [
        ("aaaaaa abd th", "a"),
        ("bbbbbbbbbbb asdf asdfo", "b"),
        ("h h h a b c d", "h"),
        ("", "")
    ])
    def test_command_get_most_frequent_char_given_a_str_should_return_the_most_frequent_char(self, command, line, expected_char):
        result = command.get_most_frequent_char(line)
        assert expected_char == result

    def test_command_handle_when_called_should_request_data_and_save_in_database(self, command, mock_requests, mock_database_save):
        command.handle()
        assert mock_requests.called
        assert mock_database_save.called
