from django.contrib import admin
from exercise2.models import RandomLine

# Register your models here.
class RandomLineAdmin(admin.ModelAdmin):
    list_display = ('id', 'most_frequent_char')

admin.site.register(RandomLine, RandomLineAdmin)
