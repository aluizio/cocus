# Installation

### First install the package manager pipenv:

    pip install pipenv

### Then install project dependencies with pipenv:

    pipenv install

# Running the server
First enter in the virtual environment created by pipenv and then run the server

    pipenv shell
    python manage.py runserver 0.0.0.0:8000

# Running the unit tests with pytest

    pytest

# Exercises
Each exercise of the challenge was splitted in Django apps with their respectives folders.

### exercise1
More details in [exercise1/README.md](exercise1/README.md)  
Access to the api data:

    http://localhost:8000/random-lines/?format=json

### exercise2
More details in [exercise2/README.md](exercise2/README.md)  
Running the script to collect and store data:

    python manage.py exercise2

### exercise3
More details in [exercise3/README.md](exercise3/README.md)  
Access to the web page rendered by django template:

    http://localhost:8000/exercise3/


# Database

The database used was Sqlite3, but Django accept diferent databases managers, like PostegreSQL, MariaDB, MySQL, Oracle and SQLite. To use one of them you need to add the connection credentials in the project file settings.py.  
The file [db.sqlite3](db.sqlite3) was kept in order to test the exercises functionalities, but it is usually ignored by Git in the file *.gitignore*.

## Credentials

The following credentials to access Django administration tool were created:

Login: cocus  
Password: cocus

The administration tool of django can be accessed in: 
http://localhost:8000/admin/