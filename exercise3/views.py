from django.views.generic import ListView
from exercise2.models import RandomLine


class Exercise3View(ListView):
    model = RandomLine
    context_object_name = 'random_lines'
    template_name = 'exercise3/exercise3.html'
    paginate_by = 5

    def get_char_frequencies(self, line: str, data: dict):
        excludes = [" ", "\n"]
        for char in line:
            if char in excludes:
                continue
            if char not in data:
                data[char]=0
            data[char] += 1
        return data

    def get_most_frequent(self):
        data = {}
        for random_line in RandomLine.objects.all():
            self.get_char_frequencies(random_line.line, data)

        max_data = max(data.items(), key=lambda x: x[1])
        return max_data[0]

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        data = {
            'most_frequent': self.get_most_frequent()
        }
        context.update(data)
        return context
