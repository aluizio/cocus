from django.urls import path
from .views import Exercise3View

urlpatterns = [
    path('exercise3/', Exercise3View.as_view()),
]
