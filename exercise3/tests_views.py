import pytest
from unittest import mock

from .views import Exercise3View, RandomLine, ListView

@pytest.mark.unit
class TestExercise3View:
    
    @pytest.fixture
    def view(self):
        return Exercise3View()

    @pytest.fixture
    def randomlines(self):
        data = [mock.Mock(), mock.Mock()]
        # most fequent is 'a'
        data[0].line = "aaaa bb c"
        data[1].line = "ccc bb a"
        return data

    @pytest.fixture
    def mock_randomline_data(self, randomlines):
        with mock.patch.object(RandomLine.objects, 'all', return_value=randomlines) as mock_method:
            yield mock_method
    
    @pytest.fixture
    def mock_super_get_context(self):
        with mock.patch.object(ListView, 'get_context_data', return_value={}) as mock_method:
            yield mock_method

    @pytest.mark.parametrize("line,expected", [
        ("aaaaaa bb c", {'a':6, 'b':2, 'c':1}),
        ("a   b  cc ", {'a':1, 'b':1, 'c':2}),
        ("", {}),
    ])    
    def test_exercise3view_get_char_frequencies_given_a_str_should_return_dict_with_frequencies(self, view, line, expected):
        result = view.get_char_frequencies(line, {})
        assert expected == result

    def test_exercise3view_get_most_frequent_should_return_the_most_frequent_char_in_database(self, view, mock_randomline_data):
        expected = 'a'
        assert expected == view.get_most_frequent()
        assert mock_randomline_data.called

    def test_exercise3view_get_context_data_should_return_dict_with_most_frequent_char_in_database(self, view, mock_randomline_data, mock_super_get_context):
        result = view.get_context_data()
        expected = {'most_frequent': 'a'}
        assert expected == result
        assert mock_super_get_context.called
        assert mock_randomline_data.called
