# Exercise \#3

This exercise has the [views.py](views.py) script, which loads and renders the HTML template. The template file is where the HTML and JavaScript code are.

## The rendered web page
The rendered web page can be accessed using the following URL:  
http://localhost:8000/exercise3/

## The button *load more*
This button was included to load more data from the server by a JavaScript request.

## Main code
The main code of the Python script is on [views.py](views.py) and the template code on [templates/exercise3/exercise3.html](templates/exercise3/exercise3.html).